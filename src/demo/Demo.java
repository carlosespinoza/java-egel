package demo;
import java.util.*;

public class Demo {
    static int posicion;
    static Scanner leer=new Scanner(System.in);
    public static void main(String[] args) {      
        //Tipos de variables
        int numero;
        char letra;
        float decimal;
        double decimalGrande;
        String cadena;
        boolean encendido;
        int [] arreglo;
        int [][] bidimencional;
        
        System.out.println("Hola Mundo Java");
        
        numero=10;
        if(numero<7){
            System.out.println("Reprobaste");
        }else{
            System.out.println("Pasaste");
        }
        for(int i=0;i<100;i++){
            System.out.println("Numero " + i+1 );
            if(i==97){
                System.out.println("Rompiendo el ciclo...");
                break;
            }
        }
        while(numero<10){
            System.out.println("Numero: "+ numero--);
        }
        do{
            System.out.println("Numero: " + numero+1);
        }while(numero>10);
        letra='j';
        switch(letra){
            case 'a':
                System.out.println("Precionaste A");
                break;
            case 'b':
            case 'B':
                System.out.println("Precionaste B");
                break;
            default:
                System.out.println("Precionaste otra letra");
                break;
        }
        //             01234567890123456789
        String nombre="Programación en Java";
        char letraN=nombre.toCharArray()[11];
        boolean letraBuscada=BuscarLetra('K',nombre);
        
        
        int edad1=13;
        int edad2=edad1;
        Persona miPersona=new Persona();
        miPersona.nombre="Juanito";
        miPersona.edad=22;
        miPersona.apellidos="López";
        edad2=12;
        Persona tuPersona=miPersona;
        tuPersona.nombre="Pedrito";
        System.out.println(edad2);
        System.out.println(miPersona.nombre);
        Empleado elEmpleado=new Empleado();
        elEmpleado.nombre="Chanito";
        elEmpleado.CumplirAnios();
        Persona otroEmpleado=new Empleado();
        otroEmpleado.CumplirAnios();
        //Empleado tuEmpleado=new Persona();
        
    }
    public static boolean BuscarLetra(char letraBuscada, String texto){
        for(int i=0;i<texto.length();i++){
            if(texto.toCharArray()[i]==letraBuscada){
                return true;
            }
        }
        return false;
    }
    public static int BuscarLetra2(char letraBuscada, String texto){
        for(int i=0;i<texto.length();i++){
            if(texto.toCharArray()[i]==letraBuscada){
                return i;
            }
        }
        return -1;
    }
    public static void BuscarLetra3(char letraBuscada, String texto){
        for(int i=0;i<texto.length();i++){
            if(texto.toCharArray()[i]==letraBuscada){
                posicion= i;
            }
        }
        posicion= -1;
    }
    
}
